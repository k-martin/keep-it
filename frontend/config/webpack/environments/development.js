'use strict';
var webpack = require('webpack');

module.exports = function(_path) {
  return {
    context: _path,
    devtool: 'source-map',
    devServer: {
      contentBase: './dist',
      hot: true,
      inline: true,
        port: 8888
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin()
    ]
  };
};
