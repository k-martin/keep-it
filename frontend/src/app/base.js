'use strict';

export default class BaseApplication {
    constructor(){
        this.angular = angular;
    }

    static get $appName() {
        return 'keepItApp'
    }
}

