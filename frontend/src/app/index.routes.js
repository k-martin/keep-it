'use strict';

class Route {
    constructor($urlRouterProvider, $stateProvider, resolverProvider){

    }
}


function routeConfig($urlRouterProvider, $stateProvider, resolverProvider) {
    // $stateProvider
    //     .state('async', {
    //       url: '/async',
    //       templateUrl: asyncTemplate,
    //       controller: 'asyncController',
    //       resolve: {
    //         asyncPreloading: resolverProvider.asyncPagePrealoading
    //       }
    //     });


  $urlRouterProvider.otherwise('/');

}

export default angular.module('index.routes', []).config(routeConfig);