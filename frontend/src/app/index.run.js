'use strict';

function runBlock($rootScope, $state, $log) {
	'ngInject';

    $rootScope.isAuth = true;

    $log.debug('Hello from run block!');

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        if(!$rootScope.isAuth) {
            if (toState.name !== 'login') {
                event.preventDefault();
                $state.go('login')
            }
        } else {
            if(toState.name === 'login'){
                $state.go('main')
            }
        }

        // return false;
    });
}

export default runBlock;
