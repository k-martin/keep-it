'use strict';

import config from './index.config';
import run from './index.run';

import uiRouter from 'angular-ui-router';

import coreModule from './core/core.module';
import indexComponents from './index.components';
import indexRoutes from './index.routes';
import loginModule from './pages/auth/login/module';
import mainModule from './pages/main/main.module';

const App = angular.module(
    "keepItApp", [
        // plugins
        uiRouter,
        "ngAnimate",
        "ngCookies",
        // "ngTouch",
        "ngSanitize",
        "ngMessages",
        "ngAria",
        "ngResource",
        "oc.lazyLoad",
        "ngMaterial",

        // core
        coreModule.name,

        // components
        indexComponents.name,

        // routes
        indexRoutes.name,

        // pages
        mainModule.name,

        loginModule.name

    ]
);
App
    .config(config)
    .run(['$rootScope','$state', '$log', run]);


// class Application extends angular.module{
//         constructor(name){
//                 super(name, Application.$dependenciesModules);
//         }
//
//         static get $inject (){
//             return ['$rootScope','$state', '$log', run];
//         }
//
//         static get $dependenciesModules(){
//             return [
//                 // plugins
//                 uiRouter,
//                 "ngAnimate",
//                 "ngCookies",
//                 // "ngTouch",
//                 "ngSanitize",
//                 "ngMessages",
//                 "ngAria",
//                 "ngResource",
//                 "oc.lazyLoad",
//                 "ngMaterial",
//
//                 // core
//                 coreModule.name,
//
//                 // components
//                 indexComponents.name,
//
//                 // routes
//                 indexRoutes.name,
//
//                 // pages
//                 mainModule.name,
//
//                 loginModule.name
//
//             ];
//         }
//
//         init() {
//             this.config(config);
//             this.run(this.$inject);
//         }
// }
//
// let App = new Application('keepItApp');
//
// App.init();

export default App;
