import BaseController from '../../core/controller';

/**
 *
 */
class HeaderController extends BaseController {

    constructor() {
        super(...arguments)

        this.$log.debug('-- Header Controller! --');
    }

    static get $inject() {
        return ['$scope', '$timeout', '$mdSidenav', '$log'];
    }

    $onInit() {
        this.$scope.toggleLeft = this.toggleSideNav.bind(this);

    }

    toggleSideNav(){
        this.$mdSidenav('left').toggle();
    }
}

export default HeaderController;