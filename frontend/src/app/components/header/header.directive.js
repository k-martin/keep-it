'use strict';

import headerTpl from './header.html';
import HeaderController from './controller';

function headerComponent($log) {
    'ngInject';
    let directive;

    directive = {
        restrict: 'E',
        templateUrl: headerTpl,
        controller: HeaderController,
        controllerAs: 'vm',
        bindToController: true
    };

  return directive;

}

export default headerComponent;
