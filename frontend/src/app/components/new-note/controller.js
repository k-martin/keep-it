import BaseController from '../../core/controller';

class NewNoteController extends BaseController {

    title;

    constructor() {
        super(...arguments)

        this.$log.debug('New note Controller!')

        this.$scope.title = 'Title';
        this.$scope.note = 'Take a note...';

    }

    static get $inject() {
        return ['$scope', '$timeout', '$log'];
    }

}

export default NewNoteController;