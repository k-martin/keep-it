'use strict';

import Component from './component';
import './new-note.scss';

const NewNoteModule = angular.module('NewNoteModule', []);

NewNoteModule.component('newNote', new Component);

export default NewNoteModule;
