'use strict';

import template from './template.html';
import controller from './controller';
import BaseDirective from '../../core/directive';

class NewNoteDirective extends BaseDirective {

    constructor() {
        super(...arguments)

        this.inject = [];

        this.restrict = 'E';
        this.templateUrl = template;
        this.controller = controller;
        this.controllerAs = 'NewNote';
        this.bindToController = true;
    }

    link(scope, iElement, iAttrs, controller, transcludeFn) {

    }

}

export default NewNoteDirective;