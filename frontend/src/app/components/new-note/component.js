import BaseComponent from '../../core/component';
import Controller from './controller';
import Template from './template.html';

class NewNoteComponent extends BaseComponent {

    constructor() {
        super(...arguments);

        this.controller = Controller;
        this.controllerAs = 'NewNote';
        this.templateUrl = Template;
    }
}

export default NewNoteComponent;
