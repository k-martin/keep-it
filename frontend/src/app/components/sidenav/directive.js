'use strict';

import template from './sidenav.html';
import controller from './controller';
import BaseDirective from '../../core/directive';

class SideNavDirective extends BaseDirective{



    constructor(){
        super(...arguments)

        this.inject = [];

        this.restrict = 'E';
        this.templateUrl = template;
        this.controller = controller;
        this.controllerAs = 'vm';
        this.bindToController = true;
    }

    link(scope, iElement, iAttrs, controller, transcludeFn) {

    }

}

export default SideNavDirective;