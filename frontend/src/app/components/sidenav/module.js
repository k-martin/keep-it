'use strict';

import Directive from './directive';
import './sidenav.scss';

const headerModule = angular.module('sidenav-module', []);

headerModule
    .directive('sidenavApp', () => new Directive);

export default headerModule;
