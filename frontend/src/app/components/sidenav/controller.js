import BaseController from '../../core/controller';

class SideNavController extends BaseController {

    constructor() {
        super(...arguments)

        this.$log.debug('Sidenav Controller!')

        this.bindWatchers();
        this.bindEvents();
    }

    static get $inject() {
        return ['$scope', '$timeout', '$log'];
    }

    buildToggler(componentId) {
        return function () {
            this.$mdSidenav(componentId).toggle();
        };
    }

    bindWatchers() {
        let me = this;
        this.$scope
            .$watch('isOpen', (isSidenavOpen) => {
                me.$scope.main.toggle(isSidenavOpen);
            });
    }

    bindEvents() {
        this.bind({
            toggleLeft: this.buildToggler('left'),
            toggleRight: this.buildToggler('right'),
            ngClickNotes: this.ngClickNotes
        });
    }

    ngClickNotes($event) {
        debugger;
    }

}

export default SideNavController;