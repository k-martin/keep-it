'use strict';

import headerModule from './components/header/header.module';
import footerModule from './components/footer/footer.module';
import sidenavModule from './components/sidenav/module';
import newNoteModule from './components/new-note/module';

export default angular.module('index.components', [
	headerModule.name,
	// footerModule.name,
    sidenavModule.name,
    newNoteModule.name
]);
