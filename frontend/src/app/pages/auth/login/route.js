'use strict';

import Tpl from './login.html';
import Controller from './controller';

function routeConfig($stateProvider) {
  'ngInject';

  $stateProvider
    .state('login', {
      url: '/login',
      templateUrl: Tpl,
      controller: Controller,
      controllerAs: 'login'
    });

}

export default routeConfig;
