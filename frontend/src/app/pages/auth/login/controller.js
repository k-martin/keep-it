'use strict';
import BaseController from '../../../core/controller'

class LoginController extends BaseController {

    constructor() {
        'ngInject';

        super(...arguments);

        this.$log.debug('Hello from main controller!');

        this.$scope.welcome = "Ciao a tutti";

        this.$scope.username = 'Ornella';

        this.$scope.password = null;


    }

    static get $inject() {
        return ['$scope', '$state', '$log'];
    }

    $onInit(){
        this.$scope.loginAction = this.login.bind(this);
    }

    login(e) {
        if(this.$scope.username && this.$scope.password){
            this.$scope.$root.isAuth = true;
            this.$state.go('main');
        }
    }
}

export default LoginController;
