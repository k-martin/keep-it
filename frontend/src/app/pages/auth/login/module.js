'use strict';

import route from './route';

const mainPageModule = angular.module('login-module', [
  'ui.router'
]);

mainPageModule
    .config(route);

export default mainPageModule;
