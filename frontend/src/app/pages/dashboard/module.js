'use strict';

import route from './route';

const Module = angular.module('dashboard-module', [
    'ui.router'
]);

Module.config(route);

export default Module;
