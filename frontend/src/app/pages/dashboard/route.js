'use strict';

import * as tpl from './dashboard.html';
import Controller from './controller';

function routeConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('dashboard', {
            url: '/dashboard',
            templateUrl: tpl,
            controller: Controller,
            controllerAs: 'dashboard'
        });

}

export default routeConfig;
