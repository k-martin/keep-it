'use strict';
import BaseController from '../../core/controller'

class LoginController extends BaseController {

    constructor() {
        'ngInject';

        super(...arguments);

        this.$log.debug('Dashboard controller!');

    }

    static get $inject() {
        return ['$scope', '$state', '$log'];
    }

    $onInit(){
        // this.$scope.loginAction = this.login.bind(this);
    }
}

export default LoginController;
