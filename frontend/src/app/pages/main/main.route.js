'use strict';

import mainTpl from './main.html';
import MainController from './controller';

function routeConfig($stateProvider) {
  'ngInject';

  $stateProvider
    .state('main', {
      url: '/',
      templateUrl: mainTpl,
      controller: MainController,
      controllerAs: 'main'
    });

}

export default routeConfig;
