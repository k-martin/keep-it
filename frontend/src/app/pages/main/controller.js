'use strict';

import BaseController from '../../core/controller';

class MainController extends BaseController {

    constructor() {
        super(...arguments)

        this.$log.debug('Main Controller!')

        this.$scope.isToggleSidenav = false;

    }

    static get $inject() {
        return ['$scope', '$log'];
    }

    toggle(isSidenavOpen) {
        this.$scope.isToggleSidenav = isSidenavOpen;
    }

}

export default MainController;
