/**
 * Base Controller Class
 *
 */
export default class BaseController {
    $scope;

    constructor(){
        let $injection = this.constructor.$inject;

        for (let i = 0; i < $injection.length; i++) {
            this[$injection[i]] = arguments[i];
        }
    }

    bind(params, callback) {
        if(params instanceof Object) {
            for (let name in params) {
                this.$scope[name] = params[name];
            }
        } else if(typeof params === 'string' && callback instanceof Function) {
            this.$scope[params] = callback;
        }

        return this;
    }
}








