'use strict';

const shared = angular.module('core.shared', ['ngSanitize']);

import validationTestDirective from './directives/validation-test/validation-test.directive';
import ContentEditableDirective from './directives/contenteditable';

import constants from './services/constants';
import storeFactory from './services/store.factory';
import resolverProvider from './services/resolver.provider';

validationTestDirective(shared);
ContentEditableDirective(shared)


constants(shared);
storeFactory(shared);
resolverProvider(shared);

export default shared;
