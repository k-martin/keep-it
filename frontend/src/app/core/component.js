/**
 * BaseComponent class
 *
 */
class BaseComponent {
    bindings;
    controller;
    controllerAs;
    require;
    template;
    templateUrl;
    transclude;

    constructor() {

    }
}

export default BaseComponent;
