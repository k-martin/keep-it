/**
 *  Base Directive Class
 */
export default class BaseDirective {

    constructor(){
        let $injection = this.constructor.$inject;

        for (let i = 0; i < $injection.length; i++) {
            this[$injection[i]] = arguments[i];
        }

    }

    static get $inject(){
        return this.inject || [];
    }
}